<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Advanced</h1>
    <br>
</p>

<p> 1. git clone https://gitlab.com/arsen.papikyan/task_smart_technologies.git</p>

<p> 2. cd  task_smart_technologies </p>

<p> 3. php init </p>
<p> 4. php yii migrate  </p>

<h1>Backend</h1>
<p> username: <strong>admin</strong> </p>
<p> password: <strong>admin1234</strong>  </p>



Задача
-------------------

```
Необходимо разработать Web приложение которое будет обращаться в  http://kgd.gov.kz/ru/app/culs-taxarrear-search-web 
по запросу пользователя, передавать введенный пользователем ИИН,решать капчу (через сервис anti-captcha) и затем парсить
полученную информацию. Распарсенные данные необходимо выводить в представление исохранять в БД при нажатии на кнопку 
“Сохранить”. Так же необходимо реализовать форму на которой можно просмотреть ранее сохраненные данные

Для решения капчи использовать: https://anti-captcha.com/apidoc 
Ключ учетной записи для anti-captcha: 

Входные данные: ИИН, капча
Пример ИИН:
791005350297
Выходные данные: Полученная информация сохраненная в таблицах БД

Для работы использовать:
1. Yii2 Framework
2. PostgreSQL
 
Создать таблицы с точки зрения целостности данных, с оптимальным распределением информации. Результат прислать в виде
ссылки на репозиторий. Особое внимание следует уделить код стайлу и оптимальности решения.

```

<?php

if (!empty($person)) {
    $date = preg_replace('~000$~sui', '', $person['sendTime']??'');
    $homeUrl = Yii::$app->homeUrl;

    ?>
    <p> Наименование налогоплательщика: <strong> <?= $person['nameRu'] ?> </strong></p>
    <p> ИИН/БИН налогоплательщика: <strong><?= $person['iinBin'] ?></strong></p>
    <p> Итого задолженности в бюджет: <strong><?= $person['totalArrear'] ?></strong></p>
    <p> Задолженность по пенсионному взносу: <strong><?= $person['pensionContributionArrear'] ?></strong></p>
    <p> Задолженность по социальному взносу: <strong><?= $person['socialContributionArrear'] ?></strong></p>
    <p> Задолженность по социальному страхованию здоровья:
        <strong><?= $person['socialHealthInsuranceArrear'] ?></strong></p>
    <br>
    <br>

    <p><strong>Орган государственных доходов <?= $person['taxOrgInfo'][0]['nameRu'] ?></strong></p>

    <p> По состоянию на: <strong> <?= date('Y-m-d', $date) ?></strong></p>
    <p> Всего задолженности: <strong><?= $person['totalArrear'] ?></strong></p>


    <form action="<?=$homeUrl?>parsers/create" method="post">
        <input type="hidden" name="nameRu" value="<?= $person['nameRu'] ?>">
        <input type="hidden" name="iinBin" value="<?= $person['iinBin'] ?>">
        <input type="hidden" name="appealledAmount" value="<?= $person['appealledAmount'] ?>">
        <input type="hidden" name="modifiedTermsAmount" value="<?= $person['modifiedTermsAmount'] ?>">
        <input type="hidden" name="rehabilitaionProcedureAmount" value="<?= $person['rehabilitaionProcedureAmount'] ?>">
        <input type="hidden" name="totalArrear" value="<?= $person['totalArrear'] ?>">
        <input type="hidden" name="pensionContributionArrear" value="<?= $person['pensionContributionArrear'] ?>">
        <input type="hidden" name="socialContributionArrear" value="<?= $person['socialContributionArrear'] ?>">
        <input type="hidden" name="socialHealthInsuranceArrear" value="<?= $person['socialHealthInsuranceArrear']  ?>">
        <input type="hidden" name="taxOrgInfo" value="<?= $person['taxOrgInfo'][0]['nameRu'] ?>">
        <input type="hidden" name="sendTime" value="<?= $date ?>">
        <input type="hidden" name="_csrf-frontend" value="<?= Yii::$app->request->getCsrfToken() ?>">

        <input type="submit" value="Сохранить">
    </form>

    <?php
}
?>

<div class="col-xs-12">
    <div class="row text-center" style="margin-top: 30%">
        <form method="post">
            <input type="text" class="form-control" name="iin" placeholder="ИИН/БИН">
            <input type="hidden" name="_csrf-frontend" value="<?= Yii::$app->request->getCsrfToken() ?>">

            <br>
            <input type="submit" class="form-control" value="search">
        </form>
    </div>
</div>



